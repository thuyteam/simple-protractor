exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['test/spec/customConfig.js','test/spec/spec.js']
    ,allScriptsTimeout: 15000
}
