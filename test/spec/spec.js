// spec.js
describe('Protractor Demo App', function() {
    //register button in home page
    var registerBtn =  element.all(by.linkText('Register')).get(0);
    var firstname =  element(by.model('vm.user.firstName'));
    var lastname =  element(by.model('vm.user.lastName'));
    var username =  element(by.model('vm.user.username'));
    var password =  element(by.model('vm.user.password'));
    //register button in register page
    var registerDetailBtn =  element.all(by.buttonText('Register')).get(0);
    //message is showed after navigating to the home page
    var message = element(by.binding('flash.message'));

    //data
    var i_firstName = 'Thuy';
    var i_lastName = 'Nguyen';
    var i_username = 'tixiuthu';
    var i_password = '123456';
    var i_successful_message = 'Registration successful';

    beforeEach(function() {
        browser.get('http://localhost/demo');
    });

    //register a user
    it('register a user', function() {
        //expectation: the system navigated to the Dashboard screen
        expect(browser.getTitle()).toEqual('Dashboard');
        //click register button
        registerBtn.click();
        //expectation: the system navigated to the Register screen
        expect( firstname.isDisplayed()).toEqual(true);
        firstname.sendKeys(i_firstName);
        lastname.sendKeys(i_lastName);
        username.sendKeys(i_username);
        password.sendKeys(i_password)
        registerDetailBtn.click().then(function () {
            expect(message.getText()).toBe(i_successful_message);
        });
    });

});